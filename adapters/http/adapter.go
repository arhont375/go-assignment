package http

import (
	"context"

	"github.com/labstack/echo"
	"gitlab.com/zenport.io/go-assignment/adapters/http/router"
	"gitlab.com/zenport.io/go-assignment/config"
	"gitlab.com/zenport.io/go-assignment/engine"
)

// HTTPAdapter - server struct
type HTTPAdapter struct {
	server *echo.Echo
}

// Start - start server
func (adapter *HTTPAdapter) Start() {
	server := adapter.server

	server.Logger.Fatal(server.Start(config.Config.ServerHost))
}

// Stop - stop server
func (adapter *HTTPAdapter) Stop() {
	adapter.server.Shutdown(context.Background())
}

// NewHTTPAdapter - create new server
func NewHTTPAdapter(engine engine.Engine) *HTTPAdapter {
	server := echo.New()

	router.InitRouter(server, engine)

	return &HTTPAdapter{server: server}
}
