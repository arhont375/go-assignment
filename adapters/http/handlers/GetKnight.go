package handlers

import (
	"net/http"

	"github.com/labstack/echo"
)

// GetKnight - list knights
func (h *Handler) GetKnight(c echo.Context) error {
	knights := h.Engine.ListKnights()

	return c.JSON(http.StatusOK, knights)
}

// GetKnightByID - return knight by ID
func (h *Handler) GetKnightByID(c echo.Context) error {
	id := c.Param("id")
	if id == "" {
		c.Logger().Error("Should provide ID")
		return echo.NewHTTPError(http.StatusBadRequest, "Should provide id")
	}

	knight, err := h.Engine.GetKnight(id)
	if err != nil {
		c.Logger().Error(err.Error())
		return echo.NewHTTPError(http.StatusNotFound, err.Error())
	}

	return c.JSON(http.StatusOK, knight)
}
