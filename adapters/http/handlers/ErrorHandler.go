package handlers

import (
	"net/http"

	"github.com/labstack/echo"
)

// CustomHTTPErrorHandler - redefine error responses structure
func CustomHTTPErrorHandler(err error, c echo.Context) {
	c.Logger().Error(err)

	response := APIResponse{Code: http.StatusInternalServerError, Message: err}
	if he, ok := err.(*echo.HTTPError); ok {
		response.Code = he.Code
		response.Message = he.Message
	}

	c.JSON(response.Code, response)
}

// APIResponse - define structure for API  responses
type APIResponse struct {
	Code    int         `json:"code"`
	Message interface{} `json:"message,omitempty"`
}
