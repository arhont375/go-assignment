package handlers

import (
	"net/http"

	"gitlab.com/zenport.io/go-assignment/engine"

	"gitlab.com/zenport.io/go-assignment/domain"

	"github.com/labstack/echo"
)

// PostKnight - add new knight to storage
func (h *Handler) PostKnight(c echo.Context) error {
	knight := &domain.Knight{}
	err := c.Bind(knight)
	if err != nil {
		c.Logger().Errorf("Failed to parse body: %s", err.Error())
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	if knight.Name == nil ||
		knight.Strength == nil ||
		knight.WeaponPower == nil {

		c.Logger().Errorf("Malformed knight: %v", knight)
		return echo.NewHTTPError(http.StatusBadRequest, "Malformed knight")
	}

	err = h.Engine.AddKnight(knight)
	if err != nil {
		if err == engine.ErrConflict {
			return echo.NewHTTPError(http.StatusConflict, "Knight already exist")
		}
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}

	return c.NoContent(http.StatusCreated)
}
