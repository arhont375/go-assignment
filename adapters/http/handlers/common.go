package handlers

import (
	"gitlab.com/zenport.io/go-assignment/engine"
)

// Handler - will keep shared objects for handlers
type Handler struct {
	Engine engine.Engine
}
