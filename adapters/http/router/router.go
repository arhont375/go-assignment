package router

import (
	"github.com/labstack/echo"
	"gitlab.com/zenport.io/go-assignment/adapters/http/handlers"
	"gitlab.com/zenport.io/go-assignment/engine"
)

func InitRouter(e *echo.Echo, engine engine.Engine) {
	handler := handlers.Handler{Engine: engine}

	e.GET("/knight", handler.GetKnight)
	e.POST("/knight", handler.PostKnight)
	e.GET("/knight/:id", handler.GetKnightByID)

	e.HTTPErrorHandler = handlers.CustomHTTPErrorHandler
}
