package domain

type Fighter interface {
	GetID() string
	GetPower() float64
}

type Knight struct {
	ID          string  `json:"id,omitempty"`
	Name        *string `json:"name"`
	Strength    *int    `json:"strength"`
	WeaponPower *int    `json:"weapon_power"`
}

func (k *Knight) GetID() string {
	return k.ID
}

func (k *Knight) GetPower() float64 {
	return (float64)(*k.Strength + *k.WeaponPower)
}
