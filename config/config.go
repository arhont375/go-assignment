package config

import (
	"os"
)

// AppConfig - application config
type AppConfig struct {
	DB          string
	ServerHost  string
	Environment string
	Migrations  string
}

// Config - contains application config
var Config = readConfig()

// readConfig - read env variables to populate config
func readConfig() *AppConfig {
	return &AppConfig{
		DB:          envOrDefault("DB", "postgres://arena:postgres@localhost:5432/postgres?sslmode=disable"),
		ServerHost:  envOrDefault("HOST", ":1323"),
		Environment: envOrDefault("ENV", "test"),
		Migrations:  envOrDefault("MIGRATIONS", "file://../../providers/database/migrations/"),
	}
}

func envOrDefault(name string, fallback string) string {
	value := os.Getenv(name)

	if len(value) == 0 {
		return fallback
	}

	return value
}
