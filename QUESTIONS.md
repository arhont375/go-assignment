# Questions

For this assignment you also have to answer a couple of questions.
There is no correct answer and none is mandatory, if you don't know just skip it.

 - **What do you think of the initial project structure ?**

    Don't have strong opinion. I just followed it. \
    Probably I will put more logic into `domain` package to form such structure:

    ```
    - domain
      - engine
        - models
        - implementation
      - knight
        - models
        - implementation 
      - other_fighter
        - models
        - implementation 
    ```

    For each of this I will make unit tests with mocking of DB. \
    Functional testing will be defined with some BDD framework, probably. Just 
    started to use them and love idea.

 - **What you will improve from your solution ?**

    Probably I would like to improve error handling, to pass errors onto another level. 

 - **For you, what are the boundaries of a service inside a micro-service architecture ?**

    Boundaries of service usually defined by pat of business for which this service responsible.

 - **For you, what are the most relevant usage for SQL, NoSQL, key-value and document store ?**

    1. SQL - perfect place for usual business data that can be found in any CRM/ERP system.
          This data is not supposed to be changed very frequently and can be stored in normalised form. \
          For example: user information with related information about roles, organization and so on. \
          Usage of RDB allow us to naturally map this domain knowledge into DB.
    1. NoSQL - not clear why it's separated options, because key-value and document store is also NoSQL databases.
          But NoSQL databases usually used for data that cann't be easily normalized or other case when we 
          will sacrifice normalization of data in order to provide faster response. Usually such databases
          allow to scale easily and have ability to tolerate outage of some nodes but can't guarantee immediate 
          consistency.
    1. KeyValue - perfect place for information that requires fast lookup. Usually such system used for 
          different sort of configuration, session management or others. 
    1. Document store - needed to provide ability for search in different not very unstructured information.  
