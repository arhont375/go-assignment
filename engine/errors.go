package engine

import (
	"errors"
	"fmt"
)

var (
	ErrNotFound  = errors.New("Not found")
	ErrMalformed = errors.New("Malformed")
	ErrConflict  = errors.New("Conflict")
)

func NewErrNotFound(id string) error {
	return fmt.Errorf("Fighter #%s not found", id)
}
