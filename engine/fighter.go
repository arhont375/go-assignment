package engine

import (
	"fmt"

	"gitlab.com/zenport.io/go-assignment/domain"
)

func (engine *arenaEngine) GetKnight(ID string) (*domain.Knight, error) {
	fighter, err := engine.knightRepository.Find(ID)
	if err != nil {
		if err == ErrNotFound {
			return nil, fmt.Errorf("Knight #%s not found", ID)
		}
		return nil, err
	}

	return fighter, nil
}

func (engine *arenaEngine) ListKnights() []*domain.Knight {
	return engine.knightRepository.FindAll()
}

func (engine *arenaEngine) Fight(fighter1ID string, fighter2ID string) (domain.Fighter, error) {
	fighter1, err := engine.knightRepository.Find(fighter1ID)
	if err != nil {
		return nil, NewErrNotFound(fighter1ID)
	}
	fighter2, err := engine.knightRepository.Find(fighter2ID)
	if err != nil {
		return nil, NewErrNotFound(fighter2ID)
	}

	return engine.arena.Fight(fighter1, fighter2), nil
}

func (engine *arenaEngine) AddKnight(knight *domain.Knight) error {
	if knight == nil {
		return ErrMalformed
	}

	return engine.knightRepository.Save(knight)
}
