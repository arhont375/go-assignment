package database

import (
	"database/sql"

	log "github.com/sirupsen/logrus"
	"gitlab.com/zenport.io/go-assignment/engine"
)

type Provider struct {
	knightsRepo *knightRepository
	session     *sql.DB
}

func (provider *Provider) GetKnightRepository() engine.KnightRepository {
	return provider.knightsRepo
}

func (provider *Provider) Close() {
	err := provider.session.Close()
	if err != nil {
		log.Errorf("Failed to close DB: %s", err.Error())
	}
}

func NewProvider() *Provider {
	session := initDB()

	return &Provider{
		session:     session,
		knightsRepo: NewKnightRepository(session),
	}
}
