package database

import (
	"database/sql"
	"os"
	"testing"

	"gitlab.com/zenport.io/go-assignment/engine"

	"github.com/stretchr/testify/assert"
	"gitlab.com/zenport.io/go-assignment/domain"
)

const (
	emptyTableSQL  = `DELETE FROM knight;`
	initialDataSQL = `
INSERT INTO knight (name, strength, weapon_power) VALUES 
	('test', 10, 100),
	('test1', 1, 10);
`
)

var (
	initialData = []*domain.Knight{
		&domain.Knight{
			ID:          "1",
			Name:        createString("test"),
			Strength:    createInt(10),
			WeaponPower: createInt(100),
		},
		&domain.Knight{
			ID:          "2",
			Name:        createString("test1"),
			Strength:    createInt(1),
			WeaponPower: createInt(10),
		},
	}
)

var (
	db         *sql.DB
	knightRepo *knightRepository
)

func TestMain(m *testing.M) {
	db = initDB()
	defer db.Close()

	knightRepo = NewKnightRepository(db)

	code := m.Run()

	ClearDB()

	os.Exit(code)
}

func TestListKnight(t *testing.T) {
	migrateUP(db)

	_, err := db.Exec(initialDataSQL)
	assert.NoError(t, err, "Should init without error")

	knights := knightRepo.FindAll()

	assert.NotNil(t, knights, "Should return knights")
	assert.Equal(t, initialData, knights, "Response from DB not equal")

	_, err = db.Exec(emptyTableSQL)
	assert.NoError(t, err, "Should clear table")

	migrateDown(db)
}

func TestListKnight_noKnights(t *testing.T) {
	migrateUP(db)

	knights := knightRepo.FindAll()

	assert.NotNil(t, knights, "Should not be nil")
	assert.Equal(t, []*domain.Knight{}, knights, "Should be empty array")

	migrateDown(db)
}

func TestInsertKnight(t *testing.T) {
	migrateUP(db)

	knight := domain.Knight{
		Name:        createString("name"),
		Strength:    createInt(100),
		WeaponPower: createInt(1000),
	}

	err := knightRepo.Save(&knight)

	assert.NoError(t, err, "Should save without error")

	res := db.QueryRow(sqlFind, 1)

	var name string
	var strength, wpower int
	err = res.Scan(&name, &strength, &wpower)

	assert.NoError(t, err, "Should scan without error")

	assert.Equal(t, knight, domain.Knight{
		Name:        &name,
		Strength:    &strength,
		WeaponPower: &wpower,
	}, "Returned knight should be the same")

	migrateDown(db)
}

func TestInsertKnight_conflict(t *testing.T) {
	migrateUP(db)

	_, err := db.Exec(initialDataSQL)
	assert.NoError(t, err, "Should init without error")

	err = knightRepo.Save(initialData[0])
	assert.Equal(t, engine.ErrConflict, err, "Should return conflict")

	migrateDown(db)
}

func TestGetKnight(t *testing.T) {
	migrateUP(db)

	_, err := db.Exec(initialDataSQL)
	assert.NoError(t, err, "Should init without error")

	knight, err := knightRepo.Find("1")
	assert.NoError(t, err, "Should get without error")

	assert.Equal(t, knight, &domain.Knight{
		ID:          "1",
		Name:        createString("test"),
		Strength:    createInt(10),
		WeaponPower: createInt(100),
	})

	migrateDown(db)
}

func TestGetKnight_notFound(t *testing.T) {
	migrateUP(db)

	_, err := db.Exec(initialDataSQL)
	assert.NoError(t, err, "Should init without error")

	knight, err := knightRepo.Find("3")
	assert.Equal(t, engine.ErrNotFound, err)

	var knightPointer *domain.Knight
	assert.Equal(t, knightPointer, knight)

	migrateDown(db)
}

func createInt(x int) *int {
	return &x
}

func createString(x string) *string {
	return &x
}
