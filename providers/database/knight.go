package database

import (
	"database/sql"

	"github.com/lib/pq"

	"gitlab.com/zenport.io/go-assignment/engine"

	log "github.com/sirupsen/logrus"
	"gitlab.com/zenport.io/go-assignment/domain"
)

type knightRepository struct {
	stmtFind    *sql.Stmt
	stmtFindAll *sql.Stmt
	stmtCreate  *sql.Stmt
}

const (
	sqlFind    = "SELECT name, strength, weapon_power FROM knight WHERE id = $1"
	sqlFindAll = "SELECT id, name, strength, weapon_power FROM knight"
	sqlCreate  = "INSERT INTO knight (name, strength, weapon_power) VALUES ($1, $2, $3)"
)

func NewKnightRepository(session *sql.DB) *knightRepository {
	stmtFind, err := session.Prepare(sqlFind)
	if err != nil {
		log.Fatalf("Failed to init Find statement: %s", err.Error())
	}
	stmtFindAll, err := session.Prepare(sqlFindAll)
	if err != nil {
		log.Fatalf("Failed to init FindAll statement: %s", err.Error())
	}
	stmtCreate, err := session.Prepare(sqlCreate)
	if err != nil {
		log.Fatalf("Failed to init Create statement: %s", err.Error())
	}

	return &knightRepository{
		stmtFind:    stmtFind,
		stmtFindAll: stmtFindAll,
		stmtCreate:  stmtCreate,
	}
}

func (repository *knightRepository) Find(id string) (*domain.Knight, error) {
	row := repository.stmtFind.QueryRow(id)

	var name string
	var strength, wpower int
	err := row.Scan(&name, &strength, &wpower)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, engine.ErrNotFound
		}
		log.Fatalf("Failed to scan knight: %s", err.Error())
	}

	return &domain.Knight{
		ID:          id,
		Name:        &name,
		Strength:    &strength,
		WeaponPower: &wpower,
	}, nil
}

func (repository *knightRepository) FindAll() []*domain.Knight {
	rows, err := repository.stmtFindAll.Query()

	if err != nil {
		log.Fatalf("Failed to query knights: %s", err.Error())
	}
	defer rows.Close()

	knights := []*domain.Knight{}
	for rows.Next() {
		var id, name string
		var strength, wpower int
		if err := rows.Scan(&id, &name, &strength, &wpower); err != nil {
			log.Fatalf("Failed to scan knight: %s", err.Error())
		}
		knights = append(knights, &domain.Knight{
			ID:          id,
			Name:        &name,
			Strength:    &strength,
			WeaponPower: &wpower,
		})
	}
	if err := rows.Err(); err != nil {
		log.Fatalf("Failed to read knights: %s", err.Error())
	}

	return knights
}

func (repository *knightRepository) Save(knight *domain.Knight) error {
	res, err := repository.stmtCreate.Exec(knight.Name, knight.Strength, knight.WeaponPower)
	if err != nil {
		if err, ok := err.(*pq.Error); ok && err.Code == "23505" {
			log.Errorf("Knight '%s' already exist: %s", knight.Name, err.Error())
			return engine.ErrConflict
		}
		log.Fatalf("Failed to insert knight: %s", err.Error())
	}
	rows, err := res.RowsAffected()
	if err != nil {
		log.Fatalf("Failed to get number of affected rows: %s", err.Error())
	}
	if rows != 1 {
		log.Fatalf("Failed to insert knight. Amount of changed rows not 1: %d", res.RowsAffected)
	}

	return nil
}
