package database

import (
	"database/sql"
	"time"

	"github.com/golang-migrate/migrate"
	"github.com/golang-migrate/migrate/database/postgres"
	_ "github.com/golang-migrate/migrate/source/file"
	_ "github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"gitlab.com/zenport.io/go-assignment/config"
)

const (
	numberOfRetries = 5
	timeToWait      = time.Second * 10
)

func initDB() *sql.DB {
	db, err := sql.Open("postgres", config.Config.DB)
	if err != nil {
		log.Fatalf("Failed to open DB connection: %s", err.Error())
	}

	err = ensureConnection(db)
	if err != nil {
		log.Fatalf("Failed to connect to DB: %s", err.Error())
	}

	migrateUP(db)

	return db
}

func migrateUP(db *sql.DB) {
	driver, err := postgres.WithInstance(db, &postgres.Config{})
	if err != nil {
		log.Fatalf("Failed to created driver for migration: %s", err.Error())
	}

	m, err := migrate.NewWithDatabaseInstance(
		config.Config.Migrations,
		"postgres",
		driver)
	if err != nil {
		log.Fatalf("Failed to read DB migrations: %s", err.Error())
	}

	err = m.Up()
	if err != nil && err != migrate.ErrNoChange {
		log.Fatalf("Failed to migrate DB: %s", err.Error())
	}
}

func migrateDown(db *sql.DB) {
	driver, err := postgres.WithInstance(db, &postgres.Config{})
	if err != nil {
		log.Fatalf("Failed to created driver for migration: %s", err.Error())
	}

	m, err := migrate.NewWithDatabaseInstance(
		config.Config.Migrations,
		"postgres",
		driver)
	if err != nil {
		log.Fatalf("Failed to read DB migrations: %s", err.Error())
	}

	err = m.Down()
	if err != nil && err != migrate.ErrNoChange {
		log.Fatalf("Failed to migrate down: %s", err.Error())
	}
}

func ClearDB() {
	if config.Config.Environment != "test" {
		log.Fatalf("Can be executed only in test environment")
	}

	db, err := sql.Open("postgres", config.Config.DB)
	if err != nil {
		log.Fatalf("Failed to open DB connection: %s", err.Error())
	}
	defer db.Close()

	err = ensureConnection(db)
	if err != nil {
		log.Fatalf("Failed to connect to DB: %s", err.Error())
	}

	migrateDown(db)
}

func ensureConnection(db *sql.DB) error {
	err := db.Ping()
	for trial := 0; err != nil && trial < numberOfRetries; trial++ {
		log.Errorf("Failed to connect to DB: %d, wait for %s seconds: %s", trial, timeToWait, err.Error())
		time.Sleep(timeToWait)
		err = db.Ping()
	}
	if err != nil {
		return err
	}

	return nil
}
