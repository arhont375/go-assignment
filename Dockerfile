FROM golang:1.10

WORKDIR /go/src/gitlab.com/zenport.io/go-assignment/
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -o arena

FROM scratch

COPY --from=0 /go/src/gitlab.com/zenport.io/go-assignment/arena .
COPY --from=0 /go/src/gitlab.com/zenport.io/go-assignment/providers/database/migrations providers/database/migrations
CMD ["./arena"]